# common utilities for sveglioo

import sarge

from sarge import capture_stdout

system = 'dev'

import os
import datetime
import time


def format_file_timestamp():
    """ Returns current timestamp as a string
    """
    return datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d--%H-%M-%S')

def format_elapsed_time(start, end):
    """ Returns the timestamp as a string
    """
    import time
    hours, rem = divmod(end-start, 3600)
    minutes, seconds = divmod(rem, 60)
    return "{:0>2}:{:0>2}:{:05.2f}".format(int(hours),int(minutes),seconds)

def format_time(t):
    """ Returns the timestamp as a string
    """
    return datetime.datetime.fromtimestamp(t).strftime('%Y-%m-%d %H:%M:%S')

out_path_relative = 'private/'

out_path = os.getcwd() + os.sep + out_path_relative 

errors_log_path = out_path + 'errors.log'

if not os.path.isdir(out_path):
    os.makedirs(out_path)

f_errors = open(errors_log_path, 'w')    

log = {
        'warn' : [],
        'error' : [],
        'fatal' : []
    }


def format_log(msg : str=""):
    return "  %s" % msg

def fatal(msg : str, ex=None):
    """ Prints error and exits (halts program execution immediatly)
    """
    if ex == None:
        exMsg = ""
    else:
        exMsg = " \n  " + repr(ex)
    s = format_log("\n\n    FATAL ERROR! %s%s\n\n" % (msg,exMsg))
    print(s)
    log['fatal'].append({'msg':s, 'ex':ex})
    f_errors.write(s)
    exit(1)

def log_error(msg : str, ex=None):
    """ Prints error but does not rethrow exception
    """
    if ex == None:
        exMsg = ""
        the_ex = Exception(msg)
    else:
        exMsg = " \n  " + repr(ex)
    the_ex = ex
    s = format_log("\n\n    ERROR! %s%s\n\n" % (msg,exMsg))
    print(s)
    log['error'].append({'msg':s, 'ex':ex})
    f_errors.write(s)

def error(msg : str, ex=None):
    """ Prints error and reraises Exception
    """
    log_error(msg, ex)
    if ex == None:
        exMsg = ""
    else:
        exMsg = " \n  " + repr(ex)
    if ex == None:
        raise Exception(exMsg)
    else:
        raise ex

def warn(msg : str="", ex=None):
    if ex == None:
        exMsg = ""
    else:
        exMsg = " \n  " + repr(ex)

    s = format_log("\n\n    WARNING: %s%s\n\n" % (msg,exMsg))
    print(s)
    log['warn'].append({'msg':s, 'ex':ex})
    f_errors.write(s)

def info(msg : str=""):
    print(format_log(msg))

def debug(msg : str=""):
    print(format_log("  DEBUG=%s" % msg))



def run(cmd : str):
    print(cmd)
    
    p = capture_stdout(cmd)
    for line in p.stdout: 
        print(repr(line))

def ask(question : str):
    return input("  %s" % question)