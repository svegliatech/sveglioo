# sveglioo

Roadmap: See [Milestones](https://gitlab.com/svegliatech/sveglioo/-/milestones)

## Prerequisites

Tested on Ubuntu 16.04

### Python 3

### docker:

docker 18.03.1-ce, build 9ee9f40

### python libraries:
```
python3 -m pip install --user sarge
```

Start dev instance

```bash
sudo docker-compose up

```


Once it is running:

- go with the browser to http://localhost:8070
- Enter 

    User=admin
    Password=admin

During first installation, go to Apps, then find module Sveglioo and install it (warning: it is a MODULE, NOT an APP).
The only thing the module does is to automatically install all the apps we are interested in.

Files created on host file system (the y persist even when docker is closed):

* db:  `/var/lib/docker/volumes/sveglioodev_db-data/_data`
* web:  `/var/lib/docker/volumes/sveglioodev_web-data/_data`


shut down dev instance (does not delete data, neither db nor file system):

```bash
sudo docker-compose down 
```

DELETE data of dev instance:

```bash
sudo ./clean-dev.py
```

# TROUBLESHOOTING

## Can't find modules in UI

a) you might have to go to Settings and activate developer mode, then go to Apps and in the upper bar click Update Apps list
 
b) it might be a permission problem, check [fix-permissions.py](fix-permissions.py)

