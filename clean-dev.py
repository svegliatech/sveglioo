#!/usr/bin/python3

import os
import sys
import common
from common import  info,warn,fatal, run, ask

if os.geteuid() != 0:
    fatal("You need to run this script as root")

info()
info("*****************      CAREFUL!       ********************")
info()
info()
warn("THIS IS GOING TO *DELETE* %s DATABASE AND FILES !" % common.system)
info()

s = ask("Are you *REALLY* sure (Y/n)?  ")
info()
if s != "Y":
    info("No change done.")
    info("Aborting.")
    info()
else:
    run("docker-compose down -v")


