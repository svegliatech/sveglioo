# -*- coding: utf-8 -*-
{
    'name': "Sveglioo",

    'summary': """
        Colleziona tutte le dipendenze di Sveglioo""",

    'description': """
        Colleziona tutte le dipendenze di Sveglioo
    """,

    'author': "Svegliatech",
    'website': "https://sveglia.tech",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [ # ODOO
                'base','project','contacts','account','hr','hr_expense','hr_holidays','stock',
                'website_sale', 'maintenance', 'hr_recruitment', 'sale_management', 'crm', 'website_event',
                'note','mail', 'website', 'calendar',
                 # OCA
                'hr_experience',
                'hr_skill',
                'project_description',
                  # OCA , gives error on install but seems so work
                  # See https://gitlab.com/svegliatech/sveglioo/issues/1
                'project_key',  
                'project_tag',
                'project_task_dependency', 
                'project_task_material',
                'project_timeline',
                'project_timeline_hr_timesheet',
                'project_timeline_task_dependency',
                'web_timeline'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}